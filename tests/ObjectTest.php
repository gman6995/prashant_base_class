<?php
/**
 * Created by PhpStorm.
 * User: Prashant
 * Date: 5/7/2018
 * Time: 8:27 AM
 */

namespace Prashant\NetTV\Base;


use PHPUnit\Framework\TestCase;

class ObjectTest extends TestCase
{
    public function testCanBeInstantiated(){
        $this->assertInstanceOf(BaseObject::class, new BaseObject());
    }

    public function testCanReturnClass(){
        $this->assertEquals('Prashant\NetTV\Base\BaseObject',(new BaseObject())->getClass());
    }

    public function testCanReturnParentClass(){
        $this->assertEquals('stdClass', (new BaseObject())->getParentClass());
    }

    public function testToCheckIfObject(){
        $this->assertTrue((new BaseObject())->isA("Prashant\NetTV\Base\BaseObject"));
    }
}