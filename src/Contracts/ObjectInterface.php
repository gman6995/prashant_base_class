<?php
/**
 * Created by PhpStorm.
 * User: Prashant
 * Date: 5/6/2018
 * Time: 6:06 PM
 */
namespace Prashant\NetTV\Base\Contracts;

/**
 * Interface ObjectInterface
 * @package Prashant\NetTV\Base
 */
interface ObjectInterface
{
    /**
     * Get name of class.
     *
     * @return string
     */
    public function getClass() ;

    /**
     * Get Parent name of class.
     *
     * @return string
     */
    public  function getParentClass() ;

    /**
     * Check if the given data is class or not
     *
     * @return bool
     */
    public function isA($class);
}