<?php
/**
 * Created by PhpStorm.
 * User: Prashant
 * Date: 5/6/2018
 * Time: 6:08 PM
 */
namespace Prashant\NetTV\Base;

use Prashant\NetTV\Base\Contracts\ObjectInterface;

class BaseObject extends \stdClass implements ObjectInterface
{

    public function getClass()
    {
        return get_class($this);
    }

    public function getParentClass()
    {
        return get_parent_class($this);
    }

    public function isA($class): Bool
    {
        return is_a($this, $class);
    }
}